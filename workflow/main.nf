#!/usr/bin/env nextflow
/** Workflow aiming at FASTA to annotation table process
  */

nextflow.enable.dsl = 2

if (!params.input) {
    error("no input sequence specified --input: dna fasta")
}

input_fasta = file(params.input);

process ANNOTATION {
    input:
    file(fasta_sequence)
    output:
    path("${fasta_sequence.baseName}.tsv")
    script:
    """
    echo "Hello, world!" > "${fasta_sequence.baseName}.tsv"
    """
}


workflow {
    ANNOTATION(input_fasta)
}
