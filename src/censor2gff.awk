#!/usr/bin/env -S awk -f
BEGIN {
	sequence = "region4"
	source = "censor"
	feature = "te"
	OFS = "\t"
}

# Avoid header
NR > 0 {
	start = $2
	end = $3
	name = $4
	class = $7
	dir = $8
	if (dir == "d") {
		orientation = "+"
	} else {
		orientation = "-"
	}
	frame = ""	# Don't specify the frame
	attributes = sprintf("class=%s;family=%s", class, name)
	print sequence, source, feature, start, end, orientation, frame, attributes
}
